#!/usr/bin/env ruby
require 'logger'

class Lockfile
	def initialize( options = { :filename => 'rb_pid.lock', :path => "./" })

		fn = options[:path]
		fn << "/" unless fn[-1] == "/"
		fn << options[:filename]

		LOGGER.info("initializing lockfile '%s'" % fn)

		if File.exist?(fn)
			LOGGER.debug("pid file exists.. ")

			pid = File.open(fn, "r") { |f| f.read.strip }
			LOGGER.debug("pid in file: %s" % pid)

			process_exists = _check_for_process(pid)
			LOGGER.debug("process status: %s" % process_exists)

			raise "Process already running (PID: %s)!" % pid if process_exists
			LOGGER.warn	("Process NOT found but PID file exists. Unexpected process termination?")
			_remove_lock(fn)
		end

		_create_lock(fn)
		@lockfile = fn
	end

	def close()
		LOGGER.info("removing PID file")
		_remove_lock(@lockfile)
	end

	def _check_for_process(pid)
			LOGGER.debug("looking for process '%s'" % pid)
			return Process.kill 0, pid.to_i

		rescue Errno::ESRCH
			LOGGER.debug("process '%s' NOT FOUND (crashed?)" % pid)
			return false
	end

	def _create_lock(fn)
		lock = Process.pid.to_s
		LOGGER.debug("creating a lock for process '%s'" % lock)

		File.open(fn, "w") do |file| 
			file.puts lock
		end
	end

	def _remove_lock(fn)
		LOGGER.debug("removing a lock file '%s'" % fn)
		File.delete(fn) if File.exist?(fn)
	end
end
