#!/usr/bin/env ruby
require './lockfile'

LOGGER = Logger.new(STDOUT)

# setting options for second logger
options = {
	:path => '/tmp',
	:filename => 'pid.lock'
}

# creating objects
a = Lockfile.new
#b = Lockfile.new # illustrating a concurrent process run

c = Lockfile.new(options)
c.close()

