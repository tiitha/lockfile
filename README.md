# lockfile

Lock file object to prevent concurrent processes from running on the same time.
Example of the usage is in `example.rb`

Just for these occasions where `(flock -w 10 200 || { echo \"Script already running\"; exit 1; }; #{app_script_file}) 200>#{app_lock_file} >> #{app_log_file} 2>&1` seems too easy. 